<?php
require 'include_new/header.php';
require 'include_new/autoloader.php';
require 'include_new/dbconnection.php';
?>
<?php
$getkey = $con->query("select * from setting")->fetch_assoc();
define('ONE_KEY',$getkey['one_key']);
define('ONE_HASH',$getkey['one_hash']);
function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
    // $resizeWidth = 100;
    // $resizeHeight = 100;
    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
    $background = imagecolorallocate($imageLayer , 0, 0, 0);
    // removing the black from the placeholder
    imagecolortransparent($imageLayer, $background);

    // turning off alpha blending (to ensure alpha channel information
    // is preserved, rather than removed (blending with the rest of the
    // image in the form of black))
    imagealphablending($imageLayer, false);

    // turning on alpha channel information saving (to ensure the full range
    // of transparency is preserved)
    imagesavealpha($imageLayer, true);
    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
    return $imageLayer;
}

function sendMessage($title){
    $content = array(
        "en" => 'New Product-'.$title
    );

    $fields = array(
        'app_id' => ONE_KEY,
        'included_segments' => array('Active Users'),
        'data' => array('type' =>1),
        'contents' => $content
    );

    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    //print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Basic '.ONE_HASH));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

function sendMessages($title){
    $content = array(
        "en" => 'Product-'.$title.'Updated'
    );

    $fields = array(
        'app_id' => ONE_KEY,
        'included_segments' => array('Active Users'),
        'data' => array('type' =>1),
        'contents' => $content
    );

    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    //print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Basic '.ONE_HASH));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}


?>

<body data-col="2-columns" class=" 2-columns ">
<div class="layer"></div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- main menu-->
    <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
    <?php include('main.php'); ?>
    <!-- Navbar (Header) Ends-->

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Import Products




                                    </h4>

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <div class="">

                                            <span >Download sample files to perform bulk action</span>
                                            <br>

                                            <a target="_blank" href="/api/productexport.php?limit=10">

                                                <button  class="btn btn-raised btn-raised btn-info">
                                                    <i cass="fa fa-check-square-o"></i> To Import New Products
                                                </button>

                                            </a>

                                            <a target="_blank" href="/api/productexport.php">

                                                <button  class="btn btn-raised btn-raised btn-info">
                                                    <i cass="fa fa-check-square-o"></i>To Update Current Product
                                                </button>

                                            </a> </div>
                                        <form class="form" action="#" method="post" enctype="multipart/form-data">
                                            <div class="form-body">




                                                <div class="form-group">
                                                    <label for="cname">Product Related File</label>
                                                    <input type="file" id="pimg" class="form-control"  placeholder="Enter Product Related Image" name="products[]" >
                                                    <p>Only Excel File supported ( .xls , .xlsx)</p>
                                                </div>



                                            </div>



                                            <div class="form-actions">

                                                <button type="submit" name="sub_product" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Update Products
                                                </button>

                                                <button type="submit" name="imp_product" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Import New Products
                                                </button>



                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>








            </div>
        </div>



    </div>
</div>

<?php
require 'include_new/js.php';
$ralation=[

    'pname'=>'Product Name',
    'sname'=>'Seller Name',
    'cid'=>'Category Id',
    'sid'=>'Sub Category Id',
    'psdesc'=>'Description',
    'pgms'=>'Product',
    'pprice'=>'Product Price',
    'stock'=>'Out of Stock',
    'discount'=>'Product Discount',
    'popular'=>'Popular',
    'status'=>'Publish'


];

if ( array_key_exists('sub_product',$_POST) && array_key_exists('products',$_FILES)) {

$file=$_FILES['products'];





if(reset($file['type'])=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){


    try {

       // $path=str_replace('\\','/',__DIR__).'/import/'.reset($file['name']);
        $path=str_replace('\\','/',__DIR__).'/import/updateProduct.xlsx';

        move_uploaded_file(reset($file['tmp_name']),$path);
      //  move_uploaded_file('update.xlsx',$path);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($path);
        $worksheet=$spreadsheet->getActiveSheet();


        $column=[];
        $tableData=[];
        $masterCount=0;
        $dif=[];
        foreach ($worksheet->getRowIterator() as $row) {

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            //    even if a cell value is not set.
            // By default, only cells that have a value
            //    set will be iterated.
            $count=0;



            foreach ($cellIterator as $cell) {
                if($masterCount==0){
                    $column[]= $cell->getValue();
                }else{
                    $tableData[$masterCount][$column[$count]]=$cell->getValue();
                }



                $count=$count+1;
            }

            $masterCount=$masterCount+1;
        }



        foreach ($tableData as $pr){

                $oldProduct=$con->query('select * from product where id='.$pr['id']);

                if($oldProduct->num_rows===1){

                    $dif[]=getOnlyNewData($oldProduct->fetch_assoc(),$pr,$ralation);

                }

              //  var_dump($oldProduct);


        }




        foreach (array_filter($dif) as $fRow){
            $sql="";
            $x=0;
            $column=$fRow;
            unset($column['id']);
            foreach ($column as $n =>$v){

                $sql.="`".$n."` = '".$v."' ";
                if(  count($column)-1 !=$x){
                    $sql.=" ,";
                }
                $x=$x+1;
            }

            $con->query('update product set '.$sql.' where id='.$fRow['id']);

        }

    ?>

    <script type="text/javascript">
        $(document).ready(function() {
            toastr.options.timeOut = 4500; // 1.5s
            toastr.info('Product Data Updated');

        });
    </script>

<?php

    }catch (Exception $e){
        error_log($e->getMessage());
    }






}else{

?>

<script type="text/javascript">
    $(document).ready(function() {
        toastr.options.timeOut = 4500; // 1.5s
        toastr.error('Opps.. Not Valid File Uploaded');

    });
</script>

<?php

    }

    //var_dump($_FILES['products']);
   // die();


}
elseif ( array_key_exists('imp_product',$_POST) && array_key_exists('products',$_FILES)){

$file=$_FILES['products'];

if(reset($file['type'])=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){



    $path=str_replace('\\','/',__DIR__).'/import/importProduct.xlsx';

    move_uploaded_file(reset($file['tmp_name']),$path) ;

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load($path);
    $worksheet=$spreadsheet->getActiveSheet();


    $column=[];
    $tableData=[];
    $masterCount=0;
    foreach ($worksheet->getRowIterator() as $row) {

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
        //    even if a cell value is not set.
        // By default, only cells that have a value
        //    set will be iterated.
        $count=0;



        foreach ($cellIterator as $cell) {
            if($masterCount==0){
                $column[]= $cell->getValue();
            }else{
                $tableData[$masterCount][$column[$count]]=$cell->getValue();
            }



            $count=$count+1;
        }

        $masterCount=$masterCount+1;
    }

    foreach ($tableData as $pr){



        $dif[]=makeKeyAsperRelation($pr,$ralation,['pname']);



        //  var_dump($oldProduct);


    }

    $dif=array_filter($dif);
    if(count($dif)>0){


        $column=array_keys(reset($dif));

        foreach ($dif as $row){



            $sqlValues='"'.implode('" , "',array_values($row)).'"';


            $sql="insert into product (". implode(",",$column) .") values ( ".$sqlValues.")";


            $con->query($sql);



        }

    }

?>

    <script type="text/javascript">
        $(document).ready(function() {
            toastr.options.timeOut = 4500; // 1.5s
            toastr.info('Products imported successfully');

        });
    </script>

    <?php





    }else{

    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            toastr.options.timeOut = 4500; // 1.5s
            toastr.error('Opps.. Not Valid File Uploaded');

        });
    </script>

    <?php
}
}


?></body>


</html>