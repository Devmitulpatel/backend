<?php
require 'include_new/header.php';
?>
<?php
function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
    // $resizeWidth = 100;
    // $resizeHeight = 100;
    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
    $background = imagecolorallocate($imageLayer , 0, 0, 0);
    // removing the black from the placeholder
    imagecolortransparent($imageLayer, $background);

    // turning off alpha blending (to ensure alpha channel information
    // is preserved, rather than removed (blending with the rest of the
    // image in the form of black))
    imagealphablending($imageLayer, false);

    // turning on alpha channel information saving (to ensure the full range
    // of transparency is preserved)
    imagesavealpha($imageLayer, true);
    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
    return $imageLayer;
}

function getNewCouponCode($count=6,$type=7,$lvl=1,$preFix=[],$dv='_'){

    $randstring=[];
//  dd($preFix);
    switch ($type) {
        case'patern':
            $characters = '456';
            break;

        case '1':
            $characters = '123456789';
            break;

        case '2':
            $characters = 'abcdefghijklmnopqrstuvwxyz';
            break;

        case '3':
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        case '4':
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        case '5':
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        case '6':
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        case '7':
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        default:
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
    }

    if($lvl>1){
        $fCode=[];
        for ($i2 = 0; $i2 <= $lvl; $i2++){
            $randstring=[];
            for ($i = 0; $i < $count; $i++) {
                $randstring[]= $characters[rand(0, strlen($characters)-1)];
            }

            $fCode[]=implode('',$randstring);


        }
        //dd($lvl);
        //dd([implode($dv,$fCode)]);
        $randstring=[implode($dv,$fCode)];
    }else{
        for ($i = 0; $i < $count; $i++) {
            $randstring[]=$characters[rand(0, strlen($characters)-1)];
        }
    }
    $randstring=implode('', $randstring);

    if(count($preFix)>0){
        //    dd($preFix);
        $preFix=implode($dv,$preFix);
        $randstring= implode($dv,[  $preFix,$randstring]);
    }

    return $randstring ;


}
function filterInputs($con,$p){
    $outArray=[];

    foreach ($p as $n=>$v){
        $outArray[$n]=mysqli_real_escape_string($con,$v);
    }
    return $outArray;

}
?>

<body data-col="2-columns" class=" 2-columns ">
<div class="layer"></div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- main menu-->
    <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
    <?php include('main.php'); ?>
    <!-- Navbar (Header) Ends-->

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->
                <?php if(isset($_GET['edit'])) {
                    $sels = $con->query("select * from pincode where id=".$_GET['edit']."");
                    $sels = $sels->fetch_assoc();
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Edit Location</h4>

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form class="form" method="post" enctype="multipart/form-data">
                                            <div class="form-body">


                                                <div class="form-group">
                                                    <label for="Location">Location Name</label>
                                                    <input type="text" id="Location" value="<?php echo $sels['location'] ?>" class="form-control"  name="Location" required >
                                                </div>

                                             <div class="form-group">
                                                    <label for="Pincode">Pincode</label>
                                                    <input type="number" id="Pincode" value="<?php echo $sels['pin'] ?>" class="form-control"  name="Pincode" required >
                                                </div>




                                            </div>

                                            <div class="form-actions">

                                                <button type="submit" name="up_cat" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>

                                            <?php
                                            if(isset($_POST['up_cat'])){
                                                $inputs=filterInputs($con,$_POST);

                                                $column=[
                                                    'pin'=>$inputs['Pincode'],
                                                    'location'=>$inputs['Location'],

                                                ];


                                                $sql="";
                                                $x=0;
                                                foreach ($column as $n =>$v){
                                                    $sql.="`".$n."` = '".$v."' ";
                                                    if(count($column)-1 !=$x){
                                                        $sql.=" ,";
                                                    }
                                                    $x=$x+1;
                                                }

                                                $sql=  "update pincode set ".$sql." where id=".$_GET['edit'];
                                               // var_dump($sql);

                                                /// die();

                                                $con->query($sql);
                                                ?>

                                                <script type="text/javascript">
                                                    $(document).ready(function() {
                                                        toastr.options.timeOut = 4500; // 1.5s

                                                        toastr.info('Pincode Update Successfully!!');
                                                        setTimeout(function()
                                                        {
                                                            window.location.href="pincodelist.php";
                                                        },1500);

                                                    });
                                                </script>

                                                <?php
                                            }
                                            ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php } else { ?>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Add Delivery Location</h4>

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form class="form" method="post" enctype="multipart/form-data">
                                            <div class="form-body">


                                                <div class="form-group">
                                                    <label for="Location">Location Name</label>
                                                    <input type="text" id="Location" value="" class="form-control"  name="Location" required >
                                                </div>

                                                <div class="form-group">
                                                    <label for="Pincode">Pincode</label>
                                                    <input type="number" id="Pincode" class="form-control"  name="Pincode" required >
                                                </div>








                                            </div>

                                            <div class="form-actions">

                                                <button type="submit" name="sub_cat" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>

                                            <?php
                                            if(isset($_POST['sub_cat'])){
                                                echo "<pre>";

                                                $inputs=filterInputs($con,$_POST);

                                                $d=   $con->query("SELECT * FROM pincode WHERE pin='".$inputs['Pincode'] ."';");
                                                if($d->num_rows > 0){
                                                    $msg="Opps Delivery Location does not Exist!!!";
                                                }else{
                                                    $column="`".implode("`,`",[
                                                            'pin',
                                                            'location'
                                                        ])."`";



                                                    $values="'".implode("','",
                                                            [
                                                                $inputs['Pincode'],
                                                                $inputs['Location'],
                                                            ])."'";


                                                    $sql="insert into pincode(".$column.")values(".$values.")";

                                                    $msg="Insert Delivery Location Successfully!!!";


                                                    $con->query($sql);

                                                }


                                                ?>

                                                <script type="text/javascript">
                                                    $(document).ready(function() {
                                                        toastr.options.timeOut = 4500; // 1.5s

                                                        toastr.info('<?php echo $msg ?>');

                                                    });
                                                </script>
                                                <?php
                                            }
                                            ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php } ?>





            </div>
        </div>



    </div>
</div>

<?php
require 'include_new/js.php';
?>


</body>


</html>