<?php
require 'include_new/header.php';
require 'include_new/helper.php';
$debug=false;
$getkey = $con->query("select * from setting")->fetch_assoc();

define('ONE_KEY',$getkey['one_key']);
define('ONE_HASH',$getkey['one_hash']);
define('r_key',$getkey['r_key']);
define('r_hash',$getkey['r_hash']);


$state=(array_key_exists('state',$_GET))?(bool)$_GET['state']:false;
$approve=(array_key_exists('approve',$_GET))?(int)$_GET['approve']:0;
$reject=(array_key_exists('reject',$_GET))?(int)$_GET['reject']:0;


if($approve!==0){
    approveRefund($_GET['approve']);
    $state=1;
}
if($reject!==0){
    rejectRefund($_GET['reject']);
}


?>
<body data-col="2-columns" class=" 2-columns ">
<div class="layer"></div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">



    <?php include('main.php'); ?>


    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->

                <section id="dom">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                    <div class="card-header">
                                        <h4 class="card-title"><?php echo ($state)?'Complete':'Pending'?> Refund List</h4>
                                    </div>
                                    <div class="card-body collapse show">
                                        <div class="card-block card-dashboard">

                                            <table class="table table-striped" id="example">
                                                <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Date</th>
                                                    <th>User Id</th>
                                                    <th>Order ID</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Preview</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                $state=(is_bool($state) && $state)?'1':'0';
                                                $sel = $con->query("select * from refund where status ='".$state."' order by uid desc");
                                                $i=0;
                                                while($row = $sel->fetch_assoc())
                                                {
                                                    $i=$i+1;
//                                                    var_dump($row);
//                                                    die();
                                                    ?>
                                                    <tr>

                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $row['date'];?></td>

                                                        <td><?php echo $row['uid'];?></td>

                                                        <td><?php echo $row['oid'];?></td>
                                                        <td><?php echo $row['amount'];?></td>
                                                        <td><?php echo ($state)?"Complete":"Pending";?></td>

                                                        <td>
                                                            <button class="preview_d btn btn-primary shadow-z-2" data-id="<?php echo $row['oid'];?>" data-toggle="modal" data-target="#myModal">Preview</button></td>


                                                        <td>

                                                            <?php if($state) {?>
                                                                <a href="#"><button class="btn shadow-z-2 btn-success" > <i class="fa fa-check"></i> Approved</button></a>
                                                            <?php }else{ ?>
                                                            <a href="?approve=<?php echo $row['oid'];?>"><button class="btn shadow-z-2 btn-success "> <i class="fa fa-check"></i> Approve</button></a>
                                                            <a href="?reject=<?php echo $row['oid'];?>"><button class="btn shadow-z-2 btn-danger "><i class="fa fa-times"></i> Reject</button></a>
                                                            <?php }?>
                                                        </td>

                                                    </tr>
                                                <?php  }?>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                </section>



            </div>
        </div>



    </div>
</div>

<?php require 'include_new/js.php';?>

</body>

<?php if($approve!==0 && !$debug){  ?>

    <script>
        window.location.replace('/refundlist.php');
    </script>

<?php  } ?>

<?php if($reject!==0  && !$debug){  ?>

    <script>
        window.location.replace('/refundlist.php');
    </script>

<?php  } ?>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">


        <div class="modal-content">
            <div class="modal-header">
                <h4>Order Preivew</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p_data">

            </div>

        </div>

    </div>
</div>

<script>
    $('#example').DataTable();
</script>
<script>
    $(document).ready(function()
    {
        $("#example").on("click", ".preview_d", function()
        {
            var id = $(this).attr('data-id');
            $.ajax({
                type:'post',
                url:'pdata.php',
                data:
                    {
                        pid:id
                    },
                success:function(data)
                {
                    $(".p_data").html(data);
                }
            });
        });
    });
</script>

<script>
    function printDiv()
    {

        var divToPrint=document.getElementById('divprint');

        var newWin=window.open('','Print-Window');
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'padding:0.5em;' +
            '}' +
            '.list-group { ' +
            ' display: flex; ' +
            ' flex-direction: column; ' +
            ' padding-left: 0; ' +
            ' margin-bottom: 0; ' +
            '}' +
            '.list-group-item {' +
            ' position: relative;' +
            'display: block;' +
            'padding: 0.75rem 1.25rem;' +
            'margin-bottom: -1px;' +
            'background-color: #fff;' +
            'border: 1px solid rgba(0, 0, 0, 0.125);' +
            '}' +

            '.float-right {' +
            'float: right !important;' +
            '}' +

            '</style>';

        newWin.document.open();
        htmlToPrint += divToPrint.innerHTML;
        newWin.document.write('<html><body onload="window.print()">'+htmlToPrint+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},1);

    }
</script>

<style>
    #example_wrapper
    {
        overflow:auto;
    }
    td p {
        /* border-bottom: 1px solid #dee2e6;*/
        /* padding: 0% !important; */
        margin: 0px;
        /* font-size:11px;*/
    }
    td.manage_td
    {
        padding: 0% !important;
    }
    table
    {
        /* font-size:12px;*/
    }
    }
</style>

</html>