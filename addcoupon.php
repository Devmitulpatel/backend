<?php
require 'include_new/header.php';
require 'include_new/helper.php';
?>
<?php
function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
    // $resizeWidth = 100;
    // $resizeHeight = 100;
    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
    $background = imagecolorallocate($imageLayer , 0, 0, 0);
    // removing the black from the placeholder
    imagecolortransparent($imageLayer, $background);

    // turning off alpha blending (to ensure alpha channel information
    // is preserved, rather than removed (blending with the rest of the
    // image in the form of black))
    imagealphablending($imageLayer, false);

    // turning on alpha channel information saving (to ensure the full range
    // of transparency is preserved)
    imagesavealpha($imageLayer, true);
    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
    return $imageLayer;
}

function getNewCouponCode($count=6,$type=7,$lvl=1,$preFix=[],$dv='_'){

$randstring=[];
//  dd($preFix);
switch ($type) {
    case'patern':
        $characters = '456';
        break;

    case '1':
        $characters = '123456789';
        break;

    case '2':
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        break;

    case '3':
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;

    case '4':
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;

    case '5':
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;

    case '6':
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;

    case '7':
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;

    default:
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;
}

if($lvl>1){
    $fCode=[];
    for ($i2 = 0; $i2 <= $lvl; $i2++){
        $randstring=[];
        for ($i = 0; $i < $count; $i++) {
            $randstring[]= $characters[rand(0, strlen($characters)-1)];
        }

        $fCode[]=implode('',$randstring);


    }
    //dd($lvl);
    //dd([implode($dv,$fCode)]);
    $randstring=[implode($dv,$fCode)];
}else{
    for ($i = 0; $i < $count; $i++) {
        $randstring[]=$characters[rand(0, strlen($characters)-1)];
    }
}
$randstring=implode('', $randstring);

if(count($preFix)>0){
    //    dd($preFix);
    $preFix=implode($dv,$preFix);
    $randstring= implode($dv,[  $preFix,$randstring]);
}

return $randstring ;


}


$allProduct=[];
$q1=$con->query("SELECT * FROM `product`");
if($q1->num_rows>0){
    while($row = $q1->fetch_assoc()){

        $allProduct[]=$row;
    }

}

$allProductJson=json_encode($allProduct,true);


?>

<body data-col="2-columns" class=" 2-columns ">
<div class="layer"></div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- main menu-->
    <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
    <?php include('main.php'); ?>
    <!-- Navbar (Header) Ends-->

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->
                <?php if(isset($_GET['edit'])) {
                    $sels = $con->query("select * from coupon where id=".$_GET['edit']."");
                    $sels = $sels->fetch_assoc();
                    if(array_key_exists('CatAllowed',$sels)){
                        $catscat=json_decode($sels['CatAllowed']);
                        $catscat=reset($catscat);
                        $catscatexplode=explode('$:',$catscat);
                        $cat=$catscatexplode[0];
                        $scat=$catscatexplode[1];
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Edit Coupon (<?php echo  $sels['code'];?>)</h4>

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form class="form" method="post" enctype="multipart/form-data">
                                            <div class="form-body">


                                                <div class="row">

                                                    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <label for="CouponCode">Coupon Code</label>
                                                        <input type="text" id="CouponCode" disabled class="form-control" value="<?php echo  $sels['code'];?>" name="CouponCode" required >
                                                        <input type="hidden"value="<?php echo  $sels['code'];?>" name="CouponCode" >
                                                    </div>

                                                    <div class="form-group col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                        <label for="CouponName">Coupon Name</label>
                                                        <input type="text" id="CouponName" class="form-control" value="<?php echo $sels['couponName'] ?>"  name="CouponName" required >
                                                    </div>



                                                </div>



                                                <div class="row">

                                                    <div class="form-group  col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <label for="DiscountType">Coupon Discount Type</label>

                                                        <select id="DiscountType" name="DiscountType" class="form-control" required  onchange="renderAccording()">
                                                            <option value="" selected="" disabled="">Select type of Discount</option>

                                                            <option value="4" <?php if($sels['DiscountType'] == 4 ) echo  "selected" ;?>>Flat Cash Discount on Product</option>
                                                            <option value="0" <?php if($sels['DiscountType'] == 0 ) echo  "selected" ;?>>Flat Cash Discount on Product Category</option>
                                                            <option value="2" <?php if($sels['DiscountType'] == 2 ) echo  "selected" ;?>>Flat Cash Discount on Total</option>
                                                            <option value="5" <?php if($sels['DiscountType'] == 5 ) echo  "selected" ;?>>Percentage Discount on Product</option>
                                                            <option value="1" <?php if($sels['DiscountType'] == 1 ) echo  "selected" ;?>>Percentage Discount on Product Category</option>
                                                            <option value="3" <?php if($sels['DiscountType'] == 3 ) echo  "selected" ;?>>Percentage Discount on Total </option>



                                                        </select>




                                                    </div>



                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="CouponDiscount">Coupon Discount</label>
                                                        <input type="number" id="CouponDiscount" class="form-control "  value="<?php echo $sels['Discount'] ?>"  name="CouponDiscount" required >
                                                    </div>
                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="MaxRedeem">Max Redeem</label>
                                                        <input type="number" id="MaxRedeem" class="form-control "  value="<?php echo $sels['LimtiUse'] ?>" name="MaxRedeem" required >
                                                        <small  class="form-text text-muted pt-1">0 for unlimited</small>
                                                    </div>


                                                    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <label for="CouponValidity">valid up to</label>
                                                        <input type="date" value="<?php echo $sels['validUpTo'] ?>" id="CouponValidity" class="form-control"  name="CouponValidity" required >
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="form-group col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="projectinput6">Select Category</label>
                                                        <select id="cat_change2" name="catname" class="form-control" >
                                                            <option value="" disabled="" <?php  if($cat==0)echo 'selected'?> >Select Category</option>
                                                            <?php
                                                            $sk = mysqli_query($con,"select * from category");
                                                            while($h = mysqli_fetch_assoc($sk))
                                                            {
                                                                ?>
                                                                <option <?php if($h['id'] == getCatOrSubCat($sels['CatAllowed']) ) echo  "selected" ;?>  value="<?php echo $h['id'];?>"><?php echo $h['catname'];?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>

                                                    <div class="form-group col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="projectinput6">Select SubCategory</label>
                                                        <select id="sub_list2" name="subcatname" class="form-control"  >
                                                            <option value=""  disabled="" <?php  if($scat==0)echo 'selected'?> >Select SubCategory</option>

                                                            <?php
                                                            if($sels['CatAllowed'] !='[]'){


                                                            $sub=$con->query("select id,name from subcategory where cat_id=".getCatOrSubCat($sels['CatAllowed']) );


                                                            if($sub->num_rows>0){

                                                                while ($row=$sub->fetch_assoc()){


                                                                    ?>
                                                                    <option value="<?php echo $row['id'] ?>" <?php if($row['id'] == getCatOrSubCat($sels['CatAllowed']) ) echo  "selected" ;?> ><?php echo $row['name'] ?></option>



                                                                <?php  }}}?>



                                                        </select>

                                                    </div>

                                                    </div>


                                                <div class="row" id="onlyForproduct">
                                                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                        <label for="productCurrent">Product name (Selected Product : <span id="totalSelectedProduct" class="text-info">0</span>) </label>
                                                        <input type="text" id="productCurrent" list="productCurrentList"  class="form-control "  value="<?php echo $sels['productCurrent'] ?>" name="productCurrent" >
                                                        <div id="productSelectedInput">

                                                        </div>

                                                        <datalist id="productCurrentList">
                                                            <?php

                                                            if(count($allProduct)>0){
                                                                foreach($allProduct as $row){

                                                                    ?>
                                                                    <option value="<?php echo $row['id']?>"><?php echo $row['pname']?> </option>



                                                                    <?php

                                                                }

                                                            }


                                                            ?>


                                                        </datalist>




                                                    </div>

                                                    <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">

                                                        <div class="btn btn-info col-12" style="margin-top:33px;cursor: pointer" onclick="addProductToList()">Add Product</div>


                                                    </div>
                                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                        <table class="table table-bordered">
                                                            <tr><th>Name</th> <th>Description</th> <th class="text-right">Discount</th><th class="text-center">Remove</th></tr>

                                                            <tbody id="productSelected">
                                                            <tr>
                                                                <td colspan="4" class="text-center">No Product Selected</td>
                                                            </tr>
                                                            </tbody>

                                                        </table>


                                                    </div>












                                                </div>





                                            </div>




                                            <div class="form-actions">

                                                <button type="submit" name="up_cat" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>

                                            <?php
                                            if(isset($_POST['up_cat'])){
                                                $inputs=filterInputs($con,$_POST);
                                              //  var_dump($inputs);
                                                $column=[
                                                        'couponName'=>$inputs['CouponName'],
                                                        'code'=>$inputs['CouponCode'],
                                                        'Discount'=>$inputs['CouponDiscount'],
                                                        'validUpTo'=>$inputs['CouponValidity'],
                                                        'DiscountType'=>$inputs['DiscountType'],
                                                        'LimtiUse'=>$inputs['MaxRedeem'],
                                                        'LimtiUse'=>json_encode([implode("$:",[$inputs['catname']??0,$inputs['subcatname']??0])],true),

                                                    ];

                                                $column['ProductAllowed']=(($inputs['DiscountType']==4||$inputs['DiscountType']==5)&&array_key_exists('productSelected',$inputs))?json_encode([$inputs['productSelected']],true):json_encode([],true);

                                                $sql="";
                                                $x=0;
                                                foreach ($column as $n =>$v){
                                                $sql.="`".$n."` = '".$v."' ";
                                                if(count($column)-1 !=$x){
                                                    $sql.=" ,";
                                                }
                                                    $x=$x+1;
                                                }

                                                $sql=  "update coupon set ".$sql." where id=".$_GET['edit'];
                                             //   var_dump($sql);

                                               /// die();

                                                $con->query($sql);
                                                ?>

                                                <script type="text/javascript">
                                                    $(document).ready(function() {
                                                        toastr.options.timeOut = 4500; // 1.5s

                                                        toastr.info('Coupon Update Successfully!!');
                                                        setTimeout(function()
                                                        {
                                                          window.location.href="couponlist.php";
                                                        },1500);

                                                    });
                                                </script>

                                                <?php
                                            }
                                            ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php }


                else { ?>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Add Coupon</h4>

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form class="form" method="post" enctype="multipart/form-data">
                                            <div class="form-body">

                                    <div class="row">

                                        <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <label for="CouponCode">Coupon Code</label>
                                            <input type="text" id="CouponCode" disabled class="form-control" value="<?php echo  getNewCouponCode();?>" name="CouponCode" required >
                                            <input type="hidden"value="<?php echo  getNewCouponCode();?>" name="CouponCode" >
                                        </div>

                                        <div class="form-group col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <label for="CouponName">Coupon Name</label>
                                            <input type="text" id="CouponName" class="form-control"  name="CouponName" required >
                                        </div>



                                    </div>



                                                <div class="row">

                                                    <div class="form-group  col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <label for="DiscountType">Coupon Discount Type</label>

                                                        <select id="DiscountType" name="DiscountType" class="form-control" required onchange="renderAccording()">
                                                            <option value="" selected="" disabled="">Select type of Discount</option>

                                                            <option value="4">Flat Cash Discount on Product</option>
                                                            <option value="0">Flat Cash Discount on Product Category</option>
                                                            <option value="2">Flat Cash Discount on Total</option>
                                                            <option value="5">Percentage Discount on Product</option>
                                                            <option value="1">Percentage Discount on Product Category</option>
                                                            <option value="3">Percentage Discount on Total </option>



                                                        </select>




                                                    </div>



                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="CouponDiscount">Coupon Discount</label>
                                                        <input type="number" id="CouponDiscount" class="form-control "  value="0" name="CouponDiscount" required >
                                                    </div>

                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="MaxRedeem">Max Redeem</label>
                                                        <input type="number" id="MaxRedeem" class="form-control "  value="0" name="MaxRedeem" required >
                                                        <small  class="form-text text-muted pt-1">0 for unlimited</small>
                                                    </div>

                                                    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <label for="CouponValidity">valid up to</label>
                                                        <input type="date" value="<?php echo  implode('-',[date('Y')+1,date('m-d')])  ?>" id="CouponValidity" class="form-control"  name="CouponValidity" required >
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="form-group col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="projectinput6">Select Category</label>
                                                        <select id="cat_change" name="catname" class="form-control" >
                                                            <option value="0" selected="" disabled="">Select Category</option>
                                                            <?php
                                                            $sk = mysqli_query($con,"select * from category");
                                                            while($h = mysqli_fetch_assoc($sk))
                                                            {
                                                                ?>
                                                                <option value="<?php echo $h['id'];?>"><?php echo $h['catname'];?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>

                                                    <div class="form-group col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                        <label for="projectinput6">Select SubCategory</label>
                                                        <select id="sub_list" name="subcatname" class="form-control"  >
                                                            <option value="0" selected="" disabled="">Select SubCategory</option>


                                                        </select>
                                                    </div>


                                                </div>




                                              <div class="row" id="onlyForproduct">
                                                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                        <label for="productCurrent">Product name (Selected Product : <span id="totalSelectedProduct" class="text-info">0</span>) </label>
                                                        <input type="text" id="productCurrent" list="productCurrentList"  class="form-control "  value="<?php echo (isset($sels) && is_array($sels) && array_key_exists('productCurrent',$sels) )?$sels['productCurrent']:'Make sure you select category & sub category where product exists.' ?>" name="productCurrent" >
                                                        <div id="productSelectedInput">

                                                        </div>

                                                        <datalist id="productCurrentList">
                                                                <?php
                                                                if(count($allProduct)>0){
                                                                    foreach($allProduct as $row){

                                                                    ?>
                                                                    <option value="<?php echo $row['id']?>"><?php echo $row['pname']?> </option>

                                                                    <?php
                                                                        $allProduct[]=$row;
                                                                        }

                                                                }


                                                                ?>


                                                        </datalist>




                                                </div>

                                                    <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">

                                                        <div class="btn btn-info col-12" style="margin-top:33px" onclick="addProductToList()">Add Product</div>


                                                    </div>
                                                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                  <table class="table table-bordered">
                                                      <tr><th>Name</th> <th>Description</th> <th class="text-right">Discount</th><th class="text-center">Remove</th></tr>

                                                      <tbody id="productSelected">
                                                    <tr>
                                                        <td colspan="4" class="text-center">No Product Selected</td>
                                                    </tr>
                                                      </tbody>

                                                  </table>


                                                  </div>












                                            </div>

                                            <div class="form-actions">

                                                <button type="submit" name="sub_cat" class="btn btn-raised btn-raised btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>

                                            <?php
                                            if(isset($_POST['sub_cat'])){
                                                echo "<pre>";

                                                $inputs=filterInputs($con,$_POST);

                                                $d=   $con->query("SELECT * FROM coupon WHERE code='".$inputs['CouponCode'] ."';");
                                                if($d->num_rows > 0){
                                                    $msg="Opps Coupon Code Exist!!!";
                                                }else{
                                                    $column="`".implode("`,`",[
                                                            'couponName',
                                                            'code',
                                                            'LimtiUse',
                                                            'Discount',
                                                            'UserAllowed',//
                                                            'ProductAllowed',//
                                                            'CatAllowed',//
                                                            'UseCount',//
                                                            'UsedArray',//
                                                            'validUpTo',
                                                            'status',//
                                                            'DiscountType',
                                                        ])."`";




                                                    $values="'".implode("','",
                                                            [
                                                                $inputs['CouponName'],
                                                                $inputs['CouponCode'],
                                                                $inputs['MaxRedeem'],
                                                                $inputs['CouponDiscount'],
                                                                json_encode([],true),
                                                                json_encode((array_key_exists('productSelected',$inputs)&&($inputs['DiscountType']==4 || $inputs['DiscountType']==5))?[$inputs['productSelected']]:[],true),
                                                                json_encode([implode("$:",[$inputs['catname']??0,$inputs['subcatname']??0])],true),
                                                                0,
                                                                json_encode([],true),
                                                                $inputs['CouponValidity'],
                                                                1,
                                                                $inputs['DiscountType']


                                                            ])."'";

                                                    $sql="insert into coupon(".$column.")values(".$values.")";

                                                    $msg="Insert Coupon Successfully!!!";

//echo $values;

                                                    //     die();

                                                    $con->query($sql);

                                                }


                                                ?>

                                                <script type="text/javascript">
                                                    $(document).ready(function() {
                                                        toastr.options.timeOut = 4500; // 1.5s

                                                        toastr.info('<?php echo $msg ?>');
                                                        window.location.href="couponlist.php";

                                                    });
                                                </script>
                                                <?php
                                            }
                                            ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php } ?>





            </div>
        </div>



    </div>
</div>
<script type="text/javascript">

    <?php  if(!array_key_exists('edit',$_GET)){?> $('#onlyForproduct').slideUp();<?php }?>

    var AllProduct=<?php echo  $allProductJson; ?>;

    var checkAccess=false;
    var lstValue="";
    var addProduct=[];


    function addProductToList(){
        var pid=$( "#productCurrent" ).val();

      //  alert(pid);
        if(pid !=""){
        $( "#productCurrent" ).val("");
        //alert(!addProduct.includes(pid));
        var checkId=function(row){
            return row.id==pid;
        }
        var allpid=AllProduct.findIndex(checkId);

        pname=AllProduct[allpid];

        //if(!addProduct.includes(pid))addProduct.push(pid);
        if(!addProduct.includes(pname.id))addProduct.push(pname.id);


            renderSelectedProduct(AllProduct,addProduct,$("#productSelected"));
   //     $("#productSelected").html(addProduct.join(','));


        }
    }

    function renderAccording() {

        var toDisaplay=$('#onlyForproduct');
        var valueToCheck=$('#DiscountType');

        switch (valueToCheck.val()) {
            case '4':
                $('#onlyForproduct').slideDown();
                break;

            case '5':
                $('#onlyForproduct').slideDown();
                break;

                default:
                $('#onlyForproduct').slideUp();
                break;



        }



    }

    function renderSelectedProduct(allProduct,selectedProduct,obj){

        var fstr=[];
        var pid=$( "#productCurrent" ).val();

        console.log(selectedProduct);

        if(selectedProduct.length>0) {
            selectedProduct.forEach(function (row) {
               // alert(row);
                var checkId=function(r){
                    return r.id==row;
                };
                var pIndex = allProduct.findIndex(checkId);
                    console.log(row);
                var product = allProduct[pIndex];
             //   alert(pIndex);
                fstr.push("<tr>");
                fstr.push('<td>' + product.pname + '</td>');
                fstr.push('<td style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 200px;" title="'+product.psdesc+'">' + product.psdesc + '</td>');
                fstr.push('<td class="text-right">' + product.discount + ' %</td>');
                fstr.push('<td class="text-center "  style="cursor: pointer;" onclick="removeProductId('+row+')"> <i class="ft-trash font-medium-3 text-danger"></i></td>');
                fstr.push("</tr>");

            });
        }else{

            fstr.push("<tr>");
            fstr.push('<td colspan="4">No Product Selected</td>');
            fstr.push("</tr>");


        }
      //  alert('Will Load');
        $('#productSelectedInput').html("<input type='hidden' name='productSelected' value='"+ selectedProduct.join('$:')+"'>");
        $('#totalSelectedProduct').html(selectedProduct.length);
        obj.html(fstr.join());


    }

    function removeProductId(pid){

        var checkId=function(r){
            return r==pid;
        };
        var pIndex = addProduct.findIndex(checkId);
        addProduct.splice(pIndex,1);
        renderSelectedProduct(AllProduct,addProduct,$("#productSelected"));

    }



</script>
<script>
    <?php  if(isset($_GET['edit'])){?>


    var valueToCheck=$('#DiscountType').val();



    var currentCoupon=<?php echo  json_encode($sels,true); ?>;



  //  alert(currentCoupon.DiscountType ==5);

    if(currentCoupon.DiscountType!=4 ||  currentCoupon.DiscountType!=5)$('#onlyForproduct').slideUp();

    if(currentCoupon.DiscountType=='4'||currentCoupon.DiscountType=='5'){

        currentCoupon.ProductAllowed=JSON.parse(currentCoupon.ProductAllowed);

        addProduct=currentCoupon.ProductAllowed[0].split('$:');

        renderAccording();
        renderSelectedProduct(AllProduct,addProduct,$("#productSelected"));

    }

    <?php } ?>
    $(document).on('change','#cat_change',function()
    {
        var value = $(this).val();
        $.ajax({
            type:'post',
            url:'getsub.php',
            data:
                {
                    catid:value
                 <?php  if(isset($_GET['edit'])){?>,  subcatid:$('#subcatname').val()<?php }?>
                },
            success:function(data)
            {

                $('#sub_list').html(data);
                $('#productCurrent').attr('value','');
            },
            error(er){
                console.log(er);
            }
        });
    });

    $(document).on('change','#cat_change2',function()
    {
        var value = $(this).val();
        $.ajax({
            type:'post',
            url:'getsub.php',
            data:
                {
                    catid:value,
                    new:true
                },
            success:function(data)
            {

                $('#sub_list2').html(data);

            }
        });
    });

</script>




<?php
require 'include_new/js.php';
?>


</body>


</html>