<?php

require str_replace('\\','/',dirname(__DIR__)).'/include/autoloader.php';



//ini_set('memory_limit', '2022M');
//phpinfo();
$filename='product-'.date("Y-m-d");

if(0){

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');


}
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

require str_replace('\\','/',dirname(__DIR__)).'/include/dbconnection.php';




use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$limit=(count($_GET)>0 && array_key_exists('limit',$_GET))?(int)$_GET['limit']:500;
//var_dump($limit);die();

$data=$con->query("select * from product where status=1 order BY id DESC LIMIT ".$limit);

$feedData=[];
$feedData2=[];



while($row=$data->fetch_assoc())
{
    $feedData['id'][]=$row['id'];
    $feedData['Product Name'][]=$row['pname'];
    $feedData['Seller Name'][]=$row['sname'];
    $feedData['Description'][]=$row['psdesc'];
    $feedData['Category Id'][]=$row['cid'];
    $feedData['Sub Category Id'][]=$row['sid'];
    $feedData['Send Notification'][]=0;
    $feedData['Publish'][]=$row['status'];
    $feedData['Popular'][]=$row['popular'];
    $feedData['Product'][]=implode(',',explode('$;',$row['pgms']));
    $feedData['Product Price'][]=implode(',',explode('$;',$row['pprice']));
    $feedData['Product Discount'][]=$row['discount'];
    $feedData['Out of Stock'][]=$row['stock'];
    $feedData['pimg'][]=$row['pimg'];


}

$data2=$con->query('select catname,id from category');
$data3=$con->query('select name,cat_id from subcategory');

while($row2=$data2->fetch_assoc()){

    $feedData2['cat'][$row2['id']]=$row2['catname'];

}
echo "<pre>";

$feedData3=[];
while ($row3=$data3->fetch_assoc()){



    $feedData3['subcat'][$row3['cat_id']][]=$row3['name'];
}

foreach ($feedData3['subcat'] as $k=>$v){

    $feedData2[$feedData2['cat'][$k]]=$v;

}







$abc=[

    'A'=>[
        'value'=>['id'],
        'width'=>5
    ],
    'B'=>[
        'value'=>['Product Name'],
        'width'=>25
    ],
    'C'=>[
        'value'=> ['Seller Name'],
        'width'=>25

    ],
    'D'=>[
        'value'=>['Description'],
        'width'=>25
    ],
    'E'=>[
        'value'=> ['Category Id'],
        'width'=>12
    ],
    'F'=>[
        'value'=> ['Sub Category Id'],
        'width'=>15
    ],
    'G'=>[
        'value'=>['Send Notification'],
        'width'=>18
    ],
    'H'=>[
        'value'=>['Publish'],
        'width'=>8
    ],
    'I'=>[
        'value'=> ['Popular'],
        'width'=>8
    ],
    'J'=>[
        'value'=>['Product'],
        'width'=>25
    ],
    'K'=>[
        'value'=> ['Product Price'],
        'width'=>25
    ],
    'L'=>[
        'value'=>['Product Discount'],
        'width'=>16
    ],
    'M'=>[
        'value'=>['Out of Stock'],
        'width'=>16
    ],
    'N'=>[
        'value'=>['pimg'],
        'width'=>16
    ],
];

$abc2=[


    'A'=>[
        'value'=>['Product Name'],
        'width'=>25
    ],
    'B'=>[
        'value'=> ['Seller Name'],
        'width'=>25

    ],
    'C'=>[
        'value'=>['Description'],
        'width'=>25
    ],
    'D'=>[
        'value'=> ['Category Id'],
        'width'=>12
    ],
    'E'=>[
        'value'=> ['Sub Category Id'],
        'width'=>15
    ],
    'F'=>[
        'value'=>['Send Notification'],
        'width'=>18
    ],
    'G'=>[
        'value'=>['Publish'],
        'width'=>8
    ],
    'H'=>[
        'value'=> ['Popular'],
        'width'=>8
    ],
    'I'=>[
        'value'=>['Product'],
        'width'=>25
    ],
    'J'=>[
        'value'=> ['Product Price'],
        'width'=>25
    ],
    'K'=>[
        'value'=>['Product Discount'],
        'width'=>16
    ],
    'L'=>[
        'value'=>['Out of Stock'],
        'width'=>16
    ],
];

//var_dump('A1:L'.count($feedData['id']));
$spreadsheet = new Spreadsheet();

//echo "pre";
//var_dump($spreadsheet);

$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle("My Excel");

$fullAbc=[];

    foreach (range('A', 'Z') as $char) {
        $fullAbc[]=$char;
    }

$fullAbc=    array_slice($fullAbc,-3,3,false);
    //var_dump($fullAbc);die();

    $masterCount=0;
    $catCount=count( $feedData2['cat']);
    $subcatCount=50;

  //  var_dump($catCount);

foreach ( $feedData2 as $colname=>$col){

    if(array_key_exists($masterCount,$fullAbc)){
        $a=$fullAbc[$masterCount];
        $count=2;

        $sheet->setCellValue($a."1", $colname);


        foreach ($col as $colrow){

            $sheet->setCellValue($a.$count, $colrow);
            $count=$count+1;
        }
        $masterCount=$masterCount+1;

    }


}

$masterCount=0;





try {
    foreach ( (count($_GET)>0 && array_key_exists('limit',$_GET))?$abc2:$abc as $c=>$cd){
        $sheet->getColumnDimension($c)->setWidth($cd['width']);
        $count=1;




        $cd['value']=array_merge($cd['value'],$feedData[reset($cd['value'])]);


//    if(reset($cd['value'])=='Product')dd($cd['value']);
//    echo "<br>";
  //   var_dump($c);
        foreach ($cd['value'] as $r){
            //var_dump($r);


                $sheet->setCellValue($c.$count, (string)$r);


if($c=='E' && $r!="Category Id"){
            $validation = $spreadsheet->getActiveSheet()->getCell($c.$count)
                ->getDataValidation();
            $validation->setType( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST );
            $validation->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION );
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1('$X$2:$X$'.($catCount+1));

}
if($c=='F' && $r!="Sub Category Id"){
            $validation = $spreadsheet->getActiveSheet()->getCell($c.$count)
                ->getDataValidation();
            $validation->setType( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST );
            $validation->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION );
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1('indirect ($E$2)');

}

            $count=$count+1;
        }



    }
    //die();


    $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    //  $writer->setOffice2003Compatibility(true);
//$writer->setPreCalculateFormulas(false);
//$writer->setOffice2003Compatibility(true);
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    // $writer->save('php://output');




    // var_dump('php://output');
    $writer->save(str_replace('\\','/',dirname(__DIR__)).'/include/'.$filename.'.xlsx');
    $newURL="/include/test.xlsx";


}catch (Exception $e){
    // var_dump($e->getMessage());
}



?>


<script>

    window.location.replace("/include/<?php echo $filename ?>.xlsx");
</script>
