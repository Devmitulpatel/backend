<?php 
require 'db.php';
require str_replace('\\','/',dirname(__DIR__)).'/include/helper.php';

$data=$_GET;
if((is_array($data) && count($data)<1))$data=$_POST;
if((is_array($data) && count($data)<1))$data=json_decode(file_get_contents('php://input'), true);

//if($data==null)$data=$_GET;
if($data==null)$data=$_POST;
if($data['uid'] == '')
{
 $returnArr = array("ResponseCode"=>"401","Result"=>"false","ResponseMsg"=>"Something Went Wrong!");
}
else
{
    $uid =  strip_tags(mysqli_real_escape_string($con,$data['uid']));
  $sel = $con->query("select * from orders where uid=".$uid." order by id desc");
  $g=array();
  $po= array();
  if($sel->num_rows != 0)
  {
  while($row = $sel->fetch_assoc())
  {
      $g=[];
      $ridre=$con->query("select * from rider where id=".$row['rid']."");

      $g['id'] = $row['id'];
      $g['oid'] = $row['oid'];
      $oid = $row['oid'];
      $g['status'] = $row['status'];
      $g['order_date'] = $row['order_date'];
	  $g['total'] = $row['total'];
      $rdata =($ridre->num_rows>0)?$ridre->fetch_assoc():[];
	  $g['rider_status'] = ($ridre->num_rows>0 && array_key_exists('r_status',$rdata))?$row['r_status']:0;
	  $g['rider_name'] = ($ridre->num_rows>0  && array_key_exists('name',$rdata))?$rdata['name']:"No Delivery Boy Assigned";
	  $g['rider_mobile'] = ($ridre->num_rows>0  && array_key_exists('mobile',$rdata))?$rdata['mobile']:0;

	  if($row['status'] === "cancelled" || $row['tid']!=0){
          $g['refund_status']=getRefundStatus($con,$row['id']);
      }else{
          $g['refund_status']=-1;
      }

      $po[] = $g;
      
  }
  $returnArr = array("Data"=>$po,"ResponseCode"=>"200","Result"=>"true","ResponseMsg"=>"Order History  Get Successfully!!!");
  }
  else 
  {
	  $returnArr = array("ResponseCode"=>"401","Result"=>"false","ResponseMsg"=>"Order  Not Found!!!");
  }
}
echo json_encode($returnArr);