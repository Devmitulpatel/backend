<?php
//
header('Content-type: text/json');

require str_replace('\\','/',dirname(__DIR__)).'/include/helper.php';

$outArray=[];

$data=$_GET;
if((is_array($data) && count($data)<1))$data=$_POST;
if((is_array($data) && count($data)<1))$data=json_decode(file_get_contents('php://input'), true);


if(array_key_exists('code',$data)){

    require 'db.php';
    $sel = $con->query("select * from coupon where code= '".$data['code']."'");


    $debug=false;
  //  var_dump($sel);


    if($sel->num_rows> 0)
    {
        http_response_code(200);
        $row=$sel->fetch_assoc();
        $userId= (array_key_exists('userid',$data))?$data['userid']:'4';


        if(!checkDateisValid($row['validUpTo']))goto Error_found;

        if((boolean)$row['LimtiUse'] !=0 && $row['LimtiUse'] <= $row['UseCount'])goto Error_found;




        $userAr=json_decode($row['UsedArray']);
        if( (boolean)$row['LimtiUse'] !=0 && in_array($userId,$userAr))goto Error_found;
        $userCount=$row['UseCount']+1;
        $userArray=(count($userAr)>0 && in_array($userId,$userAr))?$userAr :array_merge ($userAr,[$userId]);
        $userArray=json_encode($userArray,true);

        if(!$debug)$con->query("update coupon set  UseCount=".$userCount.", UsedArray='".$userArray."' where code= '".$data['code']."'");
        //var_dump($row);
        $cat=json_decode($row['CatAllowed'],true);
        $catex=explode('$:',reset($cat));
        $cat=reset($catex);
        $subcat=end($catex);
        $outArray=[
            'data'=>[
                "ResponseCode"=>200,
                "Result"=>"true",
                "ResponseMsg"=>['discount'=>$row['Discount'],'code'=>strtoupper($data['code']) ,'cat'=>$cat,'subcat'=>$subcat,'type'=>$row['DiscountType'],'msg'=>'Coupon is valid'],
            ]
        ];

        if($row['DiscountType']==4 ||$row['DiscountType']==5 ){
            $explodeAllowedProduct=json_decode($row['ProductAllowed'],true);
            $outArray['data']['ResponseMsg']['pids']=explode('$:',reset($explodeAllowedProduct));
        }



    }else{
        http_response_code(200);
        goto Error_found;
    }

}else{

    Error_found:
    $data=[];

    $outArray=[
        'data'=>[
            "ResponseCode"=>422,
            "Result"=>"false",
            "ResponseMsg"=>['msg'=>'Not Valid Code'],
        ]
    ];




}



echo json_encode($outArray,true);
?>