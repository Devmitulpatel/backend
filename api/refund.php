<?php



require str_replace('\\','/',dirname(__DIR__)).'/include/helper.php';


$outArray=[];



$data=$_GET;
if((is_array($data) && count($data)<1))$data=$_POST;
if((is_array($data) && count($data)<1))$data=json_decode(file_get_contents('php://input'), true);


if(array_key_exists('userid',$data) && ( $data['userid']!=="" || $data['userid'] !== null )){

    require 'db.php';

    $input=filterInputs($con,$data);

    if(!checkUser($input['userid']))goto Error_found;

    $outArray=[
        'data'=>[
            "ResponseCode"=>200,
            "Result"=>"true",
            "ResponseMsg"=>[
                'Refund Processed and its status is pending'
            ],
        ]
    ];


}else{
    Error_found:
    $outArray=[
        'data'=>[
            "ResponseCode"=>422,
            "Result"=>"false",
            "ResponseMsg"=>[
                'Bad Request'],
        ]
    ];
}
echo json_encode($outArray,true);