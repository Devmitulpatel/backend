<?php
header('Content-type: text/json');

require str_replace('\\','/',dirname(__DIR__)).'/include/helper.php';
$outArray=[];


$data=$_GET;
if((is_array($data) && count($data)<1))$data=$_POST;
if((is_array($data) && count($data)<1))$data=json_decode(file_get_contents('php://input'), true);


if(array_key_exists('userid',$data) && array_key_exists('type',$data) &&  array_key_exists('amount',$data)){



    require 'db.php';
    $input=filterInputs($con,$data);

    if(!checkUser($input['userid']))goto Error_found;
    ($input['type'])?addPlusEntry($con,$input['userid'],$input['amount']):addMinusEntry($con,$input['userid'],$input['amount']);

    $outArray=[
        'data'=>[
            "ResponseCode"=>200,
            "Result"=>"true",
            "ResponseMsg"=>['Wallet Updated'],
        ]
    ];

}else{
    Error_found:
    $data=[];

    $outArray=[
        'data'=>[
            "ResponseCode"=>422,
            "Result"=>"false",
            "ResponseMsg"=>['Not Valid'],
        ]
    ];


}

echo json_encode($outArray,true);
