<?php
header('Content-type: text/json');

require str_replace('\\','/',dirname(__DIR__)).'/include/helper.php';
$outArray=[];
$data=$_GET;
if((is_array($data) && count($data)<1))$data=$_POST;
if((is_array($data) && count($data)<1))$data=json_decode(file_get_contents('php://input'), true);



$withData=(array_key_exists('withHistory',$data))?(bool)$data['withHistory']:false;




if(array_key_exists('userid',$data)){


    require 'db.php';


    $input=filterInputs($con,$data);
    $sel = $con->query("select * from master_wallet where uid= '".$input['userid']."'");

    $numorow=$sel->num_rows;

    if($numorow >  0 ){




        $outArray=[
            'data'=>[
                "ResponseCode"=>200,
                "Result"=>"true",
                "ResponseMsg"=>[

                    'balance'=>$sel->fetch_assoc()['current_balance']

                ],
            ]
        ];


        if($withData){
            $table=implode('_',['wallet',$input['userid']]);
            $sql2="select * from ".$table." ORDER by id DESC limit 5";


            try {
                $data=$con->query($sql2);
            }catch (Exception $e){
                createWalletLedgerTableForuser($input['userid'],$con);
                $data=$con->query($sql2);


            }
            $his=[];
            while ($row=$data->fetch_assoc()){
                $his[]=$row;
            }



            $outArray['data']['ResponseMsg']['last5']=$his;
            $outArray['data']['ResponseMsg']['lastMsg']="Last five transaction";

        }




    }else{

        $sql="insert into master_wallet (`uid`,`current_balance`) values ('".$input['userid']."','0')";
        $con->query($sql);
        createWalletLedgerTableForuser($input['userid'],$con);

        $outArray=[
            'data'=>[
                "ResponseCode"=>200,
                "Result"=>"true",
                "ResponseMsg"=>[

                    'balance'=>0

                ],
            ]
        ];
    }

}else{
    Error_found:
    $data=[];

    $outArray=[
        'data'=>[
            "ResponseCode"=>422,
            "Result"=>"false",
            "ResponseMsg"=>[




                'Not Valid User'],
        ]
    ];


}

echo json_encode($outArray,true);
