<?php
function createWalletLedgerTableForuser($uid,$con){
    $tablename=implode('_',['wallet',$uid]);
    $sql[]="CREATE TABLE `".$tablename."` (
              `id` int(11) NOT NULL,
              `type` int(11) NOT NULL,
              `amount` float(11) NOT NULL,
              `ref` TEXT(255) NOT NULL,
              `current_balance` float(50) NOT NULL
                )";

    $sql[]="ALTER TABLE `".$tablename."`
            ADD PRIMARY KEY (`id`)";
    $sql[]="ALTER TABLE `".$tablename."`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";

    try {
        foreach ($sql as $raw){
        $con->query($raw);

        }
        addPlusEntry($con,$uid);
        return true;

    }catch (Exception $e){
        return false;
    }

}

function deleteUserWallatetable($uid,$con){
    $tablename=implode('_',['wallet',$uid]);
    $sql[]="DROP TABLE  `".$tablename."`";

    try {foreach ($sql as $raw){
        $con->query($raw);

    }
        return true;

    }catch (Exception $e){
        return false;
    }


}


function filterInputs($con,$p){
    $outArray=[];

    foreach ($p as $n=>$v){
        $outArray[$n]=mysqli_real_escape_string($con,$v);
    }
    return $outArray;

}

function mapColumn($col){


    $column="`".implode("`,`",$col)."`";


    return $column;


}

function mapValue($value){
    $outArray=[];

    foreach ($value as $v){

        switch (gettype($v)){
            case "NULL":
                $outArray[]='NULL';
                break;
            default:
                $outArray[]=implode("'",['',$v,'']);
                break;
        }

    }
    return implode(' , ',$outArray);
}

function addPlusEntry($con,$uid,$amount=0,$ref=" "){

    $sel = $con->query("select * from master_wallet where uid= '".$uid."'");
    $walleteTable=implode('_',['wallet',$uid]);
    $currentBalance=0;
    $last= $con->query("select * from ".$walleteTable)->fetch_all(MYSQLI_ASSOC);

    $last=end($last);

    //var_dump($last);

    if($last!== false){

        $currentBalance=$last['current_balance'];

    }

    $column=mapColumn([
        'type',
        'amount',
        'ref',
        'current_balance',
    ]);

   //  if($sel->num_rows<0)createWalletLedgerTableForuser($uid,$con);





     $currentBalance=$currentBalance+$amount;
    $values=mapValue([
        1,
        $amount,
        $ref,
        $currentBalance

    ]);

    $sql="insert into ".$walleteTable." (".$column.") values(".$values.")";
    $con->query($sql);

    $sql2="update master_wallet set current_balance= '".$currentBalance."' where uid= '".$uid."'";


    $con->query($sql2);




}

function addMinusEntry($con,$uid,$amount=0,$ref=" "){
    $sel = $con->query("select * from master_wallet where uid= '".$uid."'");
    $walleteTable=implode('_',['wallet',$uid]);
    $currentBalance=0;
    $last= $con->query("select * from ".$walleteTable)->fetch_all(MYSQLI_ASSOC);

    $last=end($last);


    if($last!== false){

        $currentBalance=$last['current_balance'];

    }

    $column="`".implode("`,`",[
            'type',
            'amount',
            'ref',
            'current_balance',
        ])."`";

    //if($sel->num_rows<0)createWalletLedgerTableForuser($uid,$con);




    $currentBalance=$currentBalance-$amount;
    $values=mapValue([
        0,
        $amount,
        $ref,
        $currentBalance

    ]);

    $sql="insert into ".$walleteTable." (".$column.") values(".$values.")";

    $con->query($sql);

    $sql2="update master_wallet set current_balance= '".$currentBalance."' where uid= '".$uid."'";

    $con->query($sql2);

}

function checkUser($userId){

    require 'dbconfig.php';
    //$userId=115;
    $foundUser=$con->query("select * from user where id= '".$userId."'");


    return ($foundUser->num_rows > 0)?true:false;


}

function d($data){
    if(is_null($data)){
        $str = "<i>NULL</i>";
    }elseif($data == ""){
        $str = "<i>Empty</i>";
    }elseif(is_array($data)){
        if(count($data) == 0){
            $str = "<i>Empty array.</i>";
        }else{
            $str = "<table style=\"border-bottom:0px solid #000;\" cellpadding=\"0\" cellspacing=\"0\">";
            foreach ($data as $key => $value) {
                $str .= "<tr><td style=\"background-color:#008B8B; color:#FFF;border:1px solid #000;\">" . $key . "</td><td style=\"border:1px solid #000;\">" . d($value) . "</td></tr>";
            }
            $str .= "</table>";
        }
    }elseif(is_resource($data)){
        while($arr = mysql_fetch_array($data)){
            $data_array[] = $arr;
        }
        $str = d($data_array);
    }elseif(is_object($data)){
        $str = d(get_object_vars($data));
    }elseif(is_bool($data)){
        $str = "<i>" . ($data ? "True" : "False") . "</i>";
    }else{
        $str = $data;
        $str = preg_replace("/\n/", "<br>\n", $str);
    }
    return $str;
}

function dnl($data){
    echo d($data) . "<br>\n";
}

function dd($data){
    echo dnl($data);
    exit;
}



function checkFreeShipping($uid,$oid=null){
    require str_replace('\\','/',dirname(__DIR__)).'/include/dbconnection.php';
    //require 'db.php';
    $sel = $con->query("select * from orders where uid= '".$uid."' and status != 'cancelled' ORDER BY `id` ASC");
    if($oid==null) return  ($sel->num_rows ===  1 )? true :false ;
    $first=$sel->fetch_assoc();

    return ($oid==$first['id'])?true:false;



}


function printCategoryWithSub($data){
    require 'include_new/dbconnection.php';
    $data=json_decode($data,true);
    $t="";
    if(count($data)>0){

        $f=reset($data);

        $fexplode=explode('$:',$f);
        $catid=reset($fexplode);
        $subcatid=end($fexplode);

        $sql1="select * from category where id=".$catid;
        $cat=$con->query($sql1)->fetch_assoc()['catname'];

        $sql2="select * from subcategory where cat_id=".$catid." and  id=".$subcatid;
        $subcat=$con->query($sql2)->fetch_assoc()['name'];
        $t=implode(" -> ",[$cat,$subcat]);


    }else{
        $t= "No Category Selected";
    }

    echo $t;
    //var_dump($data);

}

function getCatOrSubCat($data=[],$type=0){
    $r=null;
    $data=json_decode($data,true);

    if(count($data)>0){

        switch ($type){
            case 0:
                $dataExplode=explode("$:",reset($data));
                 $r=reset($dataExplode);

            break;
            case 1:
                $dataExplode=explode("$:",reset($data));
                $r=end($dataExplode);
                break;
        }


    }

    return $r;

}

function checkDateisValid($date){
    $exD=explode('-',$date);
    $y=$exD[0];
    $m=$exD[1];
    $d=$exD[2];
    $cy=date('Y');
    $cm=date('m');
    $cd=date('d');
    if($cy >= $y)return false;
    if($cy==$y && $cm >= $m)return false;
    if($cy==$y && $cm==$m  &&$cd >= $d)return false;

    return true;

}

function getOnlyNewData($oldData=[],$newData=[],$relation=[]){

    $outArray=[];
    foreach ($relation as $tk=>$nk){
        if($tk=='pgms' || $tk=='pprice'){
            $newData[$nk]=implode('$;',explode(',',$newData[$nk]));
        }

        if($oldData[$tk] != $newData[$nk])$outArray[$tk]=$newData[$nk];
    }
    if( count($outArray)>0 )$outArray['id']=$oldData['id'];
    return $outArray;

}

function makeKeyAsperRelation($newData=[],$relation=[],$uniq=[]){

    require "include_new/dbconnection.php";
    $outArray=[];


    foreach ($relation as $tk=>$nk){
        if($tk=='pgms' || $tk=='pprice'){
            $newData[$nk]=implode('$;',explode(',',$newData[$nk]));
        }

        $sql=[];

        $outArray[$tk]=$newData[$nk];


    }


    if(count($uniq)>0)foreach ($uniq as $tk){

        $sql[]=$tk."='".$outArray[$tk]."'";

    }
    $sql=implode(' or ',$sql);

    $found=$con->query('select * from product where '.$sql);



    if($found->num_rows <  1 ) {

        return $outArray;
    }else{
     return [];
    }




}


function addRefund($oid){
    require str_replace('\\','/',dirname(__DIR__)).'/include/dbconnection.php';
    $sql="select * from orders where id = '". $oid."'";

    $foundOrder=$con->query($sql)->fetch_assoc();

    $column=mapColumn([
        'uid',
        'oid',
        'amount',
        'status',
        'date'
            ] );

    $values=mapValue([
        $foundOrder['uid'],
        $oid,
        $foundOrder['total'],
        0,
        date('Y-m-d')
    ]);


    $sql="insert into refund (".$column.") values(".$values.")";
    return $con->query($sql);


}





function approveRefund($oid){
    require str_replace('\\','/',dirname(__DIR__)).'/include/dbconnection.php';



    $sql2="select * from refund where id = '".$oid."'";
    $refund=$con->query($sql2)->fetch_assoc();

    try{
        addPlusEntry($con,$refund['uid'],$refund['amount'],$refund['oid']);
    }catch (Exception $c){

        createWalletLedgerTableForuser($refund['uid'],$con);
        addPlusEntry($con,$refund['uid'],$refund['amount'],$refund['oid']);
    }


    $sql="update refund set status=1 where id= '".$oid."'";

    $con->query($sql);



}

function rejectRefund($oid){
    require str_replace('\\','/',dirname(__DIR__)).'/include/dbconnection.php';

    $sql="delete from refund where id= '".$oid."'";

    $con->query($sql);

}

function getRefundStatus($con,$oid){

    $sql="select * from refund where oid= '".$oid."'";

    $foundRefund=$con->query($sql);
//    var_dump($foundRefund);

    if($foundRefund->num_rows>0){

        $foundRe=$foundRefund->fetch_assoc();

        return ($foundRe['status'])?1:0;


    }else{
        return 2;
    }




}